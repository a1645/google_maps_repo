import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Center(
        child: Stack(
          children: [
            SpinKitRing(
              color: Colors.lightGreen,
              size: 51,
              lineWidth: 4,
            ),
            Center(
              child: Text(
                "Loading",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
