//<Program>****************************************************************/
//* Author.............: Lian Kruger
//* Program Name.......: map_screen.dart
//* Program Description: Statless Widget that displays Google Maps
//* Functionality......: - Loads current location on initialisation and positions map to current location, also adds marker to current location
//                       - Floating Action button that onPressed loads current position on initialisation and positions map to current location
//*                      - Tap and hold onScreen to add Markers for 'Origin' and 'Destination'
//*                      - Respective 'Origin' and 'Destination' Textbutton on Appbar that onPressed positions map to respective marker location (*If 'Origin' or 'Destination' Marker is not Null)
//* Date...............: 01 August 2021
//****************************************************************/
//imports
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_lian_kruger/Screens/loading_screen.dart';

//<Statefull Widget MapScreen>***********************/
class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

//<Statefull Widget State>***************************************************************************************/
//* Author..............: Lian Kruger
//* State Name..........: _MapScreenState (extends MapScreen)
//* State description...: Main Screen UI that displays google maps and contains functionality
//* Date................: 01 August 2021
//************************************************************************************************/
class _MapScreenState extends State<MapScreen> {
  //Varablies
  CameraPosition? _initialCameraPosition =
      CameraPosition(target: LatLng(37.773972, -122.431297), zoom: 11.5);
  GoogleMapController? _googleMapController;
  Marker? _origin;
  Marker? _destination;

  //function called on initialisation
  @override
  void initState() {
    //Loads current position on initialisation and positions map to current location, also adds marker to current location
    getCurrentLocation();
  }

  //function called on disposal
  @override
  void dispose() {
    _googleMapController!.dispose();
    super.dispose();
  }

  //Main Widget Build
  Widget build(BuildContext context) {
    if (_initialCameraPosition == null) {
      return LoadingScreen();
    } else {
      return Scaffold(
        appBar: AppBar(
          centerTitle: false,
          title: const Text('Lian\'s Google Maps'),
          actions: [
            if (_origin != null)
              //Adds Origin Text Button to Appbar that onPressed positions screen position to the Origin Marker (*Only if Origin Marker exists)
              TextButton(
                onPressed: () => _googleMapController!.animateCamera(
                  CameraUpdate.newCameraPosition(
                    CameraPosition(
                        target: _origin!.position, zoom: 14.5, tilt: 50.0),
                  ),
                ),
                style: TextButton.styleFrom(
                  primary: Colors.green,
                  textStyle: const TextStyle(fontWeight: FontWeight.w600),
                ),
                child: const Text('ORIGIN'),
              ),
            if (_destination != null)
              //Adds Destination Text Button to Appbar that onPressed positions screen position to the Destination Marker (*Only if Destination Marker exists)
              TextButton(
                onPressed: () => _googleMapController!.animateCamera(
                  CameraUpdate.newCameraPosition(
                    CameraPosition(
                        target: _destination!.position, zoom: 14.5, tilt: 50.0),
                  ),
                ),
                style: TextButton.styleFrom(
                  primary: Colors.green,
                  textStyle: const TextStyle(fontWeight: FontWeight.w600),
                ),
                child: const Text('DESTINATION'),
              )
          ],
        ),
        body: GoogleMap(
          // Google Maps Widget that displays Google Maps on Screen
          myLocationButtonEnabled: false,
          zoomControlsEnabled: false,
          initialCameraPosition: _initialCameraPosition!,
          onMapCreated: (controller) => _googleMapController = controller,
          markers: {
            if (_origin != null) _origin!,
            if (_destination != null) _destination!
          },
          onLongPress: _addMarker,
        ),
        //Floating Action button that onPressed loads current position on initialisation and positions map to current location
        floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).primaryColor,
          foregroundColor: Colors.black,
          onPressed: () => getCurrentLocation(),
          child: const Icon(Icons.center_focus_strong),
        ),
      );
    }
  }

//<Function>***************************************************************************************/
//* Author..............: Lian Kruger
//* Function Name.......: _addMarker
//* Function description: add Markers for 'Origin' and 'Destination'
//* Date...............: 01 August 2021
//* Parameters : LatLng pos (A latitude and Longitude position where the marker needs to be added)
//************************************************************************************************/
  void _addMarker(LatLng pos) {
    if (_origin == null || (_origin != null && _destination != null)) {
      //Set Origin Marker if a destination Marker already exists and reset Destitination Marker
      setState(() {
        _origin = Marker(
            markerId: const MarkerId('origin'),
            infoWindow: const InfoWindow(title: 'Origin'),
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
            position: pos);
        //Reset destination Marker
        _destination = null;
      });
    } else {
      //Set Destitination Marker if a Origin Marker already exists and a Destitination Marker does nots
      setState(() {
        _destination = Marker(
            markerId: const MarkerId('destination'),
            infoWindow: const InfoWindow(title: 'Destination'),
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
            position: pos);
      });
    }
  }

//<Function>***************************************************************************************/
//* Author..............: Lian Kruger
//* Function Name.......: getCurrentLocation
//* Function description: Loads current location and positions map to current location, also adds marker to current location
//* Date...............: 01 August 2021
//* Parameters.........: *None
//************************************************************************************************/
  void getCurrentLocation() async {
    //variables
    CameraPosition? _currentLocation;
    //determine current location
    Position _position = await _determinePosition();
    //Set Camera position target to determined location
    _currentLocation = CameraPosition(
        target: LatLng(_position.latitude, _position.longitude), zoom: 11.5);
    //setState to update statefull widget
    setState(() {
      //Google Maps animation for position change to current location
      _googleMapController!
          .animateCamera(CameraUpdate.newCameraPosition(_currentLocation!));
      //Create an origin Marker at current location
      _origin = Marker(
          markerId: const MarkerId('myLocation'),
          infoWindow: const InfoWindow(title: 'My Location'),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          position: _currentLocation.target);
    });
  }

//<Function>***************************************************************************************/
//* Author..............: Lian Kruger
//* Function Name.......: _determinePosition
//* Function description: Determines current location using Geolocator dependency.
//*                       - Also asks Permission to use location if location services are disbled on device
//* Date...............: 01 August 2021
//* Parameters.........: *None
//* Returns............: Position (Current location of device)
//*                      - Returns Future.error in case of failure
//************************************************************************************************/
  Future<Position> _determinePosition() async {
    //variables
    bool serviceEnabled;
    LocationPermission permission;

    //check if location service is enabled (Open settings if not so that user can enable it)
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    }

    //check if location permission granted
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are deied.');
      }
    }

    //check if location permission is denied forever
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied. Continueing without initial current location');
    }

    //if no issues, return current position from Geolocator
    return await Geolocator.getCurrentPosition();
  }
}
