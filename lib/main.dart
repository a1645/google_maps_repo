//<Program>****************************************************************/
//* Author.............: Lian Kruger
//* Program Name.......: main.dart
//* Program Description: Main program that calls the MapScreen
//* Date...............: 01 August 2021
//****************************************************************/
//imports
import 'package:flutter/material.dart';
import 'Screens/map_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lian Google Maps',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: MapScreen(),
    );
  }
}
